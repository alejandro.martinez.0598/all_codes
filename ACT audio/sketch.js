// Objeto para el micro , objeto para grabar , y archivo de sonido 
var mic, recorder;
var soundFile= [];
// estado del programa 
var state = 0;
//posicion de corte en el arreglo 
var pos = 0;
var amp = 0;
var sil = 0;
var ind = 0;
var op= false;
var tol = 40;
function setup() {
  background(200);
  createCanvas(200,200)
  // crea entrada de audio
  mic = new p5.AudioIn();
  // pide permiso para usar micro e inicia
  mic.start();
  // crea la grabadora 
  recorder = new p5.SoundRecorder();
  // conecta el microfono a la grabadora " entre muchas comillas"
  recorder.setInput(mic);
  // crea el ACHIVO  de sonido en memoria temporal
  soundFile [0] = new p5.SoundFile ();
  
  // traquea la amplitud de el sonido reproducido 
  amp = new p5.Amplitude();
}
function draw (){
 if (state === 0  && mic.enabled)
 {
    background(200,100,100);
    text('picale para grabar', 20, 20);
 }
  else if (state ===1  && mic.enabled) 
  {
    background(255,0,0);
    text('Grabando!', 20, 20);
    text(mic.getLevel()*200, 20, 40);
    ellipse(100,100,200,mic.getLevel()*200)
    if(!op && mic.getLevel()*200>tol)
    {
       op = true;
       amp.setInput(soundFile[sil]);
       recorder.record(soundFile[sil]);
       sil++;
       soundFile [sil] = new p5.SoundFile ();
    }
    else if(op && mic.getLevel()*200<tol)
    {
      op = false;
      recorder.stop();
    }
     text(sil, 100, 20);
  }
  else if (state == 2) 
  {
    background(0,255,0);
    text('parado', 20, 20);
    text(sil, 100, 20);
  }

  else if (state == 3) 
  {
   pos = 0;
   background(0,100,100);
   text('reproduciendo :,)', 20, 20);
   ellipse(100,100,200,amp.getLevel()*200);
   text(ind, 20, 40);
      
  }
 
}

function mousePressed() {

  if (state === 0 && mic.enabled) 
  {
  // crea el ACHIVO  de sonido en memoria temporal
  soundFile [0] = null
  soundFile [0] = new p5.SoundFile ();
  for(var i = 1 ; i<sil;i++)
  {
  soundFile [i] = null;
  }
   sil = 0;
   ind = 0;
    //amp.setInput(soundFile[sil]);
    // comienza a grabar dentro  de soundfile
    //recorder.record(soundFile[sil]);
    state++;
  }
  else if (state == 1) 
  {
    background(0,255,0);

    //corta la grabacion y lo guarda en sounfile
    //recorder.stop();
    //recordLv[ind]  = soundFile[ind].getPeaks();
    
    state++;
  }

  else if (state == 2)
  {
    state++;
  }
  else if (state == 3)
  {
   state=0;
   if(soundFile.isPlaying())
    {
       soundFile[ind].stop();    
    }
    
  
  }
}


function keyPressed() {
if (state == 3)
{
  if (keyCode === LEFT_ARROW)
  {
    ind--;
  } else if (keyCode === RIGHT_ARROW)
  {
    ind++;
  }
  else if (keyCode === UP_ARROW)
  {
    soundFile[ind].play();
  }
  else if (keyCode === DOWN_ARROW)
  {
         
    save(soundFile[ind], 'LOL.wav');
    
    //sibir al server
  }
  
  //save(soundFile,'nombre.wav');
  
}
       
}