var video;
function setup() {
  createCanvas(320,240);
  pixelDensity(1);
  video = createCapture(VIDEO);
  video.size(320,240);
}

function draw() {
background(51);
 loadPixels();
 video.loadPixels();
 for(var x = 0;x<width;x++)
 {
   for(var y = 0; y<height;y++)
   {
    var index = (x+y*width) * 4;
    
    pixels[index+0]= video.pixels[index+0];
    pixels[index+1]= video.pixels[index+1];
    pixels[index+2]= video.pixels[index+2];
    pixels[index+3]= 255;
    
   }
 }
 updatePixels();
 
 
 

}