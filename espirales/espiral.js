function Espiral()
{
var Vpos = [] ;
Vpos[0]=0;
this.poli = 3;
this.rat = 0.05;
this.vueltas = 30;
this.points = 0;
var a,b,d,f;
  this.rellena = function()
  {
   this.poli = this.points;
     while(this.points <this.poli*this.vueltas){
        //saca x
        this.a = Vpos[(this.points-1)*2];
        this.b = Vpos[(this.points-this.poli)*2];
        this.d = -((this.b-this.a)*this.rat) + this.b;
        Vpos[this.points*2]  = this.d; 
        //saca y
         this.a = Vpos[((this.points-1)*2)+1];
        this.b = Vpos[((this.points-this.poli)*2)+1];
        this.d = -((this.b-this.a)*this.rat) +this.b;
        Vpos[(this.points*2)+1]= this.d; 
       this.points++; 
    }
  }
  this.punto = function()
  {
    Vpos[2*this.points]=mouseX;
    Vpos[(2*this.points)+1]=mouseY;
    this.points++;
  }
  
  this.dib = function()
  {
     noFill();
  stroke(200)
  beginShape();
  for(var i = 0 ; i<Vpos.length; i+=2)
    vertex(Vpos[i],Vpos[i+1]);
  endShape();
  }
  
}