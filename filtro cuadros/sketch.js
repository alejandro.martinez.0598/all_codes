var vid;
var scl = 8;
function setup() {
  createCanvas(640, 420);
  vid = createCapture(VIDEO);
  vid.size(80,60);
  rectMode(CENTER);
 
}
function draw() {
  background(51);
  vid.loadPixels();
  for (var y = 0; y < height; y += scl) {
    for (var x = 0; x < width; x += scl) {
      var offset = ((y*width)+x)*4;
     noStroke()
      bright = (vid.pixels[offset]+vid.pixels[offset+1]+vid.pixels[offset+2])/200;
     // fill(vid.pixels[offset], vid.pixels[offset+1],vid.pixels[offset+2]);
    //ellipse(x, y, scl/bright, scl/bright);
   
      rect(x, y, scl/(bright+20),scl/(bright+20)); 
    }
  }
}